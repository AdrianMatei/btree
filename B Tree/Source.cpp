#include<iostream>

struct NODE
{
	int n;
	int* key;
	bool leaf;
	NODE** children;

	bool last = 0;

	NODE(int t)
	{
		n = 0;

		key = new int[2 * t - 1];
		for (int index = 0; index < 2 * t - 1; index++)
			key[index] = 0;

		leaf = true;

		children = new NODE*[2 * t];
		for (int index = 0; index < 2 * t; index++)
			children[index] = NULL;

	}

	/*~NODE()
	{
		delete[]key;
		delete[]children;
	}*/

};

struct MyQueue {

	NODE** elements;
	int size;
	int last, first;

	MyQueue()
	{
		size = 500;
		first = 0;
		last = 0;
		elements = new NODE*[size];
	}

	bool ISEMPTY()
	{
		if (first == last)
			return 1;
		return 0;
	}

	bool ISFULL()
	{
		if (last == first - 1)
			return 1;
		return 0;
	}

	void PUSH(NODE* elem)
	{
		if (ISFULL())
			std::cout << " Coada e plina \n";
		else
		{
			elements[last] = elem;
			if ((last + 1) % size == first)
				last = 0;
			else
				last++;
		}
	}

	void POP()
	{
		if (ISEMPTY())
		{
			std::cout << " Coada e goala \n";
			first = 0;
		}
		else
			first++;
	}

};

struct B_Tree
{
	NODE* root;
	int t;

	B_Tree()
	{
		root = NULL;
	}

	void INSERT(NODE* X, int newKey)
	{
		if (root == NULL)
		{
			root = new NODE(t);
			root->key[0] = newKey;
			root->n++;
		}
		else
		{
			NODE* R = root;
			if (R->n == 2 * t - 1)
			{
				NODE* S = new NODE(t);
				root = S;
				S->leaf = false;
				S->children[0] = R;
				SPLIT(S, 0);
				INSERT_NONFULL(S, newKey);
			}
			else
				INSERT_NONFULL(R, newKey);
		}
	}

	void SPLIT(NODE* X, int index)
	{
		NODE* Z = new NODE(t);
		NODE* Y = X->children[index];
		Z->leaf = Y->leaf;
		Z->n = t - 1;
		for (int i = 0; i < t - 1; i++)
			Z->key[i] = Y->key[i + t];
		if (Y->leaf == false)
			for (int i = 0; i < t; i++)
				Z->children[i] = Y->children[i + t];
		Y->n = t - 1;
		for (int i = X->n + 1; i >= index + 1; i--)
			X->children[i + 1] = X->children[i];
		X->children[index + 1] = Z;
		for (int i = X->n; i >= index; i--)
			X->key[i + 1] = X->key[i];
		X->key[index] = Y->key[t - 1];
		X->n++;
		Y->last = 0;
	}

	void INSERT_NONFULL(NODE* X, int newKey)
	{
		int index = X->n - 1;
		if (X->leaf == true)
		{
			while (index >= 0 && newKey < X->key[index])
			{
				X->key[index + 1] = X->key[index];
				index--;
			}
			X->key[index + 1] = newKey;
			X->n++;
		}
		else
		{
			while (index >= 0 && newKey < X->key[index])
				index--;
			index++;
			if (X->children[index]->n == 2 * t - 1)
			{
				SPLIT(X, index);
				if (newKey > X->key[index])
					index++;
			}
			INSERT_NONFULL(X->children[index], newKey);
		}
	}

	NODE* SEARCH(NODE* X, int k)
	{
		if (X == NULL)
			return NULL;
		int index = 0;
		while (index < X->n && X->key[index] < k)
			index++;
		if (index < X->n && X->key[index] == k)
			return X;
		else
			if (X->leaf == true)
				return NULL;
			else
				return SEARCH(X->children[index], k);
	}

	void DELETE(NODE* X, int k)
	{
		int pozition = 0;
		while (pozition < X->n && X->key[pozition] < k)
			pozition++;
		if (pozition < X->n && X->key[pozition] == k)
		{
			if (X->leaf == true) // Cazul I
			{
				for (int index = pozition; index < X->n - 1; index++)
					X->key[index] = X->key[index + 1];
				X->n--;
			}
			else // Cazul II
			{
				NODE* Y = X->children[pozition];
				NODE* Z = X->children[pozition + 1];
				if (Y->n >= t) // II a)
				{
					int predecessor = PREDECESSOR(X, pozition);
					DELETE(X->children[pozition], predecessor);
					X->key[pozition] = predecessor;
				}
				else
				{
					if (Z->n >= t) // II b)
					{
						int successor = SUCCESSOR(X, pozition);
						DELETE(X->children[pozition + 1], successor);
						X->key[pozition] = successor;
					}
					else // II c)
					{
						X = FUSION(X, pozition);
						std::cout << "\n FUZIUNE:";
						PRINT(root);
						DELETE(X, k);
					}
				}
			}
		}
		else
		{
			if (X->leaf == false) // Cazul III
			{
				if (X->children[pozition]->n == t - 1)
				{
					if (pozition < X->n && X->children[pozition + 1]->n >= t)
					{
						ROTATE_LEFT(X, pozition);
						std::cout << "\n ROTATIE LA STANGA:";
						PRINT(root);
						DELETE(X, k);
					}
					else
					{
						if (pozition > 0 && X->children[pozition - 1]->n >= t)
						{
							ROTATE_RIGHT(X, pozition);
							std::cout << "\n ROTATIE LA DREAPTA:";
							PRINT(root);
							DELETE(X, k);
						}
						else
						{
							X = FUSION(X, pozition);
							std::cout << "\n FUZIUNE:";
							PRINT(root);
							DELETE(X, k);
						}
					}
				}
				else
					DELETE(X->children[pozition], k);
			}
		}
	}

	int PREDECESSOR(NODE* X, int index)
	{
		X = X->children[index];
		while (X->leaf == false)
			X = X->children[X->n];
		return X->key[X->n - 1];
	}

	int SUCCESSOR(NODE* X, int index)
	{
		X = X->children[index + 1];
		while (X->leaf == false)
			X = X->children[0];
		return X->key[0];
	}

	void ROTATE_RIGHT(NODE* X, int pozition)
	{
		NODE* Y = X->children[pozition - 1];
		NODE* Z = X->children[pozition];
		int k = X->key[pozition - 1];
		X->key[pozition - 1] = Y->key[Y->n - 1];
		Z->n++;
		for (int index = Z->n - 1; index > 0; index--)
			Z->key[index] = Z->key[index - 1];
		Z->key[0] = k;
		for (int index = Z->n; index > 0; index--)
			Z->children[index] = Z->children[index - 1];
		Z->children[0] = Y->children[Y->n];
		Y->n--;
	}

	void ROTATE_LEFT(NODE* X, int pozition)
	{
		NODE* Y = X->children[pozition];
		NODE* Z = X->children[pozition + 1];
		int k = X->key[pozition];
		X->key[pozition] = Z->key[0];
		for (int index = 0; index < Z->n - 1; index++)
			Z->key[index] = Z->key[index + 1];
		Z->n--;
		Y->key[Y->n] = k;
		Y->n++;
		for (int index = Y->n - 1; index > 0; index--)
			Y->children[index] = Y->children[index - 1];
		Y->children[Y->n] = Z->children[0];
	}

	NODE* FUSION(NODE* X, int pozition)
	{
		NODE* Y;
		NODE* Z;
		if (pozition == X->n)
		{
			Y = X->children[pozition - 1];
			Z = X->children[pozition];
		}
		else
		{
			Y = X->children[pozition];
			Z = X->children[pozition + 1];
		}
		if (pozition == X->n)
			Y->key[t - 1] = X->key[pozition - 1];
		else
			Y->key[t - 1] = X->key[pozition];
		Y->n = Y->n + 1 + Z->n;
		for (int i = 0; i < Z->n; i++)
			Y->key[i + t] = Z->key[i];
		for (int i = 0; i <= Z->n; i++)
			Y->children[i + t] = Z->children[i];
		for (int index = pozition + 1; index < X->n; index++)
			X->key[index - 1] = X->key[index];
		for (int index = pozition + 2; index <= X->n; index++)
			X->children[index - 1] = X->children[index];
		X->n--;
		if (X->n == 0)
			root = Y;
		delete[]Z;
		return Y;
	}

	void FILL(NODE* X, int pozition, int k)
	{
		//int k = X->key[pozition];
		if (X->children[pozition]->n >= t)
			ROTATE_RIGHT(X, pozition);
		else
			if (X->children[pozition + 1]->n >= t)
				ROTATE_LEFT(X, pozition);
			else
			{
				FUSION(X, pozition);
				DELETE(X, k);
			}
	}

	void PRINT(NODE* X)
	{
		last(root);
		std::cout << "\n--------------------------------------------------\n";
		if (X != NULL)
		{
			MyQueue Q;
			Q.PUSH(X);
			std::cout << "[";
			for (int index = 0; index < X->n; index++)
			{
				std::cout << X->key[index];
				if (index != X->n - 1)
					std::cout << " ";
			}
			std::cout << "]\n";
			while (!Q.ISEMPTY() && X != NULL)
			{
				for (int index = 0; index < X->n + 1; index++)
				{
					NODE* Y = X->children[index];
					if (Y != NULL)
					{
						Q.PUSH(Y);
						std::cout << "[";
						for (int index = 0; index < Y->n; index++)
						{
							std::cout << Y->key[index];
							if (index != Y->n - 1)
								std::cout << " ";
						}
						std::cout << "]     ";
						if (Y->last == 1)
							std::cout << "\n";
					}
				}
				Q.POP();
				X = Q.elements[Q.first];
			}
		}
		std::cout << "\n--------------------------------------------------\n\n";
	}

	void last(NODE* X)
	{
		while (X != NULL && X->leaf == false)
		{
			X->last = 1;
			X = X->children[X->n];
			//std::cout << X->key[0] << "\n";
		}
	}

};

void menu()
{
	std::cout << "\n 1. Inserare element";
	std::cout << "\n 2. Cautare element";
	std::cout << "\n 3. Stergere element";
	std::cout << "\n 4. Inchidere program";
	std::cout << "\n";
	std::cout << "\n 5. Inserare primele n numere naturale";
	std::cout << "\n 6. Inserare n elemente";
	std::cout << "\n\n Optiune: ";
}

int main()
{
	B_Tree BT;
	std::cout << "\n T = ";
	std::cin >> BT.t;
	int option;
	int n, val;
	NODE* X;
	do {
		system("CLS");
		BT.PRINT(BT.root);
		menu();
		std::cin >> option;
		switch (option)
		{
			case 1: std::cout << "\n\n\n Elementul de inserat: ";
					std::cin >> val;
					X = new NODE(BT.t);
					BT.INSERT(X, val);
					break;

			case 2: std::cout << "\n\n\n Elementul de cautat: ";
					std::cin >> val;
					if (BT.SEARCH(BT.root, val) != NULL)
						std::cout << " Elementul a fost gasit!\n\n";
					else
						std::cout << " Elementul NU a fost gasit!\n\n";
					system("pause");
					break;

			case 3: std::cout << "\n\n\n Elementul de sters: ";
					std::cin >> val;
					system("CLS");
					X = BT.SEARCH(BT.root, val);
					if (X != NULL)
					{
						BT.PRINT(BT.root);
						BT.DELETE(BT.root, val);
						std::cout << "\n STERGERE: ";
						BT.PRINT(BT.root);
						std::cout << "\n Elementul a fost sters!\n\n";
					}
					else
						std::cout << "\n Elementul NU a fost gasit!\n\n";
					system("pause");
					break;

			case 5: std::cout << "\n\n\n N = ";
					std::cin >> n;
					for (int index = 1; index <= n; index++)
						BT.INSERT(BT.root, index);
					break;

			case 6: std::cout << "\n\n\n N = ";
					std::cin >> n;
					for (int index = 0; index < n; index++)
					{
						std::cin >> val;
						BT.INSERT(BT.root, val);
					}
					break;

		}
	} while (option != 4);
	return 0;
}
//1 7 1 9 1 11 1 10 1 6 1 22 1 16 1 19 1 17 1 15 1 8
//7 9 11 10 6 22 16 19 17 15 8
// 1 3 7 10 11 13 14 15 18 16 19 24 25 26 21 4 5 20 22 2 17 12 6